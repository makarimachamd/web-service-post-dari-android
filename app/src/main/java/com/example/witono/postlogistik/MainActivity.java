package com.example.witono.postlogistik;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.witono.postlogistik.Preview.Preview;
import com.example.witono.postlogistik.Service.ApiService;
import com.example.witono.postlogistik.Service.ApiUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    private TextView mResponseTv;
    private ApiService mAPIService;
    private static final String TAG = "PreviewActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText namabarang = findViewById(R.id.namabarang);
        final EditText jenis = findViewById(R.id.jenis);
        final EditText harga = findViewById(R.id.harga);
        final EditText tujuan = findViewById(R.id.tujuan);
        final EditText asal = findViewById(R.id.et_asal);
        final EditText status = findViewById(R.id.status);
        Button submitBtn = (Button) findViewById(R.id.btn_submit);
        mResponseTv = (TextView) findViewById(R.id.tv_response);

        mAPIService = ApiUtils.getAPIService();

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String barang = namabarang.getText().toString().trim();
                String jeniss = jenis.getText().toString().trim();
                String hargaa = harga.getText().toString().trim();
                String tujuann = tujuan.getText().toString().trim();
                String asall = asal.getText().toString().trim();
                String statuss = status.getText().toString().trim();

                if(!TextUtils.isEmpty(barang) && !TextUtils.isEmpty(jeniss)) {
                    sendPost(barang, jeniss, hargaa,tujuann,asall,statuss);
                }
            }
        });
    }
    public void sendPost(String barang, String jenis,String harga, String tujuan,String asal, String status) {
        mAPIService.savePost(barang, jenis,harga,tujuan,asal,status, String.valueOf(1)).enqueue(new Callback<Preview>() {
            @Override
            public void onResponse(Call<Preview> call, Response<Preview> response) {
                if(response.isSuccessful()) {
                    showResponse(response.body().toString());
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Preview> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    public void showResponse(String response) {
        if(mResponseTv.getVisibility() == View.GONE) {
            mResponseTv.setVisibility(View.VISIBLE);
        }
        mResponseTv.setText(response);
    }
}
