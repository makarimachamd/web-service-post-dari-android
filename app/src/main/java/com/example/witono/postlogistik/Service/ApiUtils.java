package com.example.witono.postlogistik.Service;

import com.example.witono.postlogistik.Helper.RetrofitClientBantu;

public class ApiUtils {
    private ApiUtils() {}

    public static final String BASE_URL = "https://tcckelompokfmna.000webhostapp.com/";

    public static ApiService getAPIService() {

        return RetrofitClientBantu.getClient(BASE_URL).create(ApiService.class);
    }
}
